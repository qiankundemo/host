import Vue from "vue";
import App from "./App.vue";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import VueRouter from "vue-router";
import { routes } from "./router";
import { registerMicroApps, start } from "qiankun";
import store from "./store";

registerMicroApps([
  {
    name: "op",
    entry: "//localhost:8081",
    container: "#container_op",
    activeRule: (location) => {
      const { hash } = location;
      return hash.indexOf("/op/") > -1;
    },
  },
  {
    name: "develop",
    entry: "//localhost:8082",
    container: "#container_develop",
    activeRule: "/develop",
  },
  {
    name: "projectmanage",
    entry: "//localhost:8083",
    container: "#container_projectmanage",
    activeRule: "/projectmanage",
  },
  {
    name: "vueApp",
    entry: "//localhost:8091",
    container: "#container_vue",
    activeRule: (location) => {
      const { hash } = location;
      return hash.indexOf("/vue/") > -1;
    },
  },
]);
Vue.use(VueRouter);
Vue.config.productionTip = false;
Vue.use(ElementUI);

const router = new VueRouter({
  mode: "hash",
  routes,
});
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

start();
