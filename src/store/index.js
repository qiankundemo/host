import Vuex from "vuex";
import Vue from "vue";
Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    globalIndex: "1",
  },
  mutations: {
    updateGlobalIndex(state, index) {
      state.globalIndex = index;
    },
  },
});

export default store;
