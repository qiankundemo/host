const DEV = "http://168.61.17.179:8080";
const SIT0 = "http://168.61.18.195:8080";
const SIT = "http://168.61.17.177";
const API_PATH = SIT0;
module.exports = {
  // publicPath: "/platformapp/dubhe",
  // pages: {
  //   index: {
  //     entry: "src/Index/main.js",
  //     template: "public/index.html",
  //     filename: "index.html",
  //     title: "Index Page",
  //     chunks: ["chunk-vendors", "chunk-common", "index"],
  //   },
  //   home: {
  //     entry: "src/Home/main.js",
  //     template: "public/index.html",
  //     filename: "home.html",
  //     title: "Home Page",
  //     chunks: ["chunk-vendors", "chunk-common", "home"],
  //   },
  //   detail: {
  //     entry: "src/Detail/main.js",
  //     template: "public/index.html",
  //     filename: "detail.html",
  //     title: "Detail Page",
  //     chunks: ["chunk-vendors", "chunk-common", "detail"],
  //   },
  // },

  devServer: {
    proxy: {
      "/platformapp/dubhe/menu": {
        target: API_PATH,
      },
      "/paas/platformapp/dubhe/i": {
        target: API_PATH,
        pathRewrite: {
          "^/paas/platformapp/dubhe/i": "/platformapp/dubhe/i", //重写接口
        },
      },
      "/platformapp/dubhe/i": {
        target: API_PATH,
      },
      "/DSP2": {
        target: "http://168.61.70.89:20000",
        secure: false,
      },
      // 代理配置-20210413-tao
      "/opt3": {
        //target: "http://168.63.65.181:8081",
        target: "http://168.61.12.131:8082",
        secure: false,
      },
    },
  },
};
